package ru.wandmagic;

import java.lang.reflect.Array;
import java.util.Random;

public class Generator {
    private final static int FRAME_OF_INT =  6000000;

    @SuppressWarnings("unchecked")
    public static <T extends Number> T[] array(Class<T> clazz, int elements) {
        Random rnd = new Random(150);
        // Use Array native method to create array
        // of a type only known at run time
        final T[] array = (T[]) Array.newInstance(clazz, elements);
        for (int i = 0; i < elements; i++) {
            array[i] = (T) generateValue(rnd, clazz);
        }
        return array;
    }

    private static Object generateValue(Random rnd, Class<? extends Number> clazz) {
        String className = clazz.getSimpleName();
        switch (className) {
            case "Integer":
                return rnd.nextInt(FRAME_OF_INT);
            case "Float":
                return rnd.nextFloat();
            default:
                throw new IllegalArgumentException("Class name '" + className + "' does not support.");
        }
    }
}
