package ru.wandmagic;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import java.util.concurrent.TimeUnit;


@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.SECONDS)
@Fork(value = 1, warmups = 2)
@Warmup(iterations = 3)
@Measurement(iterations = 5)
public class JMHtests {

    @State(Scope.Thread)
    public static class Parameters {
        private Integer[] intArray;
        private Float[] fltArray;

        @Setup(Level.Invocation)
        public void init() {
            intArray = Generator.array(Integer.class, 50000);
            fltArray = Generator.array(Float.class, 50000);
        }
    }

    @Benchmark
    public void testMergeSortInt(Parameters param, Blackhole bh) {
        Sort.mergeSort(param.intArray);
        bh.consume(param.intArray);
    }

    @Benchmark
    public void testMergeSortFlt(Parameters param, Blackhole bh) {
        Sort.mergeSort(param.fltArray);
        bh.consume(param.fltArray);
    }

    @Benchmark
    public void testBubbleSortInt(Parameters param, Blackhole bh) {
        Sort.bubbleSort(param.intArray);
        bh.consume(param.intArray);
    }

    @Benchmark
    public void testBubbleSortFlt(Parameters param, Blackhole bh) {
        Sort.bubbleSort(param.fltArray);
        bh.consume(param.fltArray);
    }

    @Benchmark
    public void testChoseSortInt(Parameters param, Blackhole bh) {
        Sort.chooseSort(param.intArray);
        bh.consume(param.intArray);
    }

    @Benchmark
    public void testChoseSortFlt(Parameters param, Blackhole bh) {
        Sort.chooseSort(param.fltArray);
        bh.consume(param.fltArray);
    }

    @Benchmark
    public void testInsertionSortInt(Parameters param, Blackhole bh) {
        Sort.insertionSort(param.intArray);
        bh.consume(param.intArray);
    }

    @Benchmark
    public void testInsertionSortFlt(Parameters param, Blackhole bh) {
        Sort.insertionSort(param.fltArray);
        bh.consume(param.fltArray);
    }

    @Benchmark
    public void testQuickSortInt(Parameters param, Blackhole bh) {
        Sort.quickSort(param.intArray);
        bh.consume(param.intArray);
    }

    @Benchmark
    public void testQuickSortFlt(Parameters param, Blackhole bh) {
        Sort.quickSort(param.fltArray);
        bh.consume(param.fltArray);
    }
}
