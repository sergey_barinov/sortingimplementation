package ru.wandmagic;


import org.openjdk.jmh.runner.RunnerException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;


public class Main {

    public static void main(String... args) {

        try {
            org.openjdk.jmh.Main.main(args);
        } catch (RunnerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Method> methods = new ArrayList<>();

        for(Method m : Sort.class.getMethods()) {
            if (m.getName().contains("Sort")) {
                methods.add(m);
            }
        }

        testSort(Generator.array(Integer.class, 100), methods);
        testSort(Generator.array(Float.class, 100), methods);
    }

    private static <T extends Number & Comparable<T>> void testSort(T[] arr, List<Method> methods) {
        out.print("Source arr:  ");
        printArr(arr);

        for (Method m : methods) {
            out.printf("%15s:  ", m.getName());
            T[] copyArr = arr.clone();
            try {
                m.invoke(null, (Object)copyArr);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            printArr(copyArr);
        }
    }

    private static <T> void printArr(T[] arr) {
        out.print("[");
        for (T element : arr) {
            out.print(element + ", ");
        }
        out.println("]");
    }
}
