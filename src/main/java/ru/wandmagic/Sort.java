package ru.wandmagic;


import java.util.Arrays;

public class Sort {

    public static <T extends Number & Comparable<T>> T[] mergeSort(T[] arr) {
        int arrSize = arr.length;
        if (arrSize <= 1) {
            return arr;
        }

        T[] arr1 = mergeSort(Arrays.copyOfRange(arr, 0, arrSize / 2));
        T[] arr2 = mergeSort(Arrays.copyOfRange(arr, arrSize / 2, arrSize));

        int count1 = 0;
        int count2 = 0;
        int size1 = arr1.length - 1;
        int size2 = arr2.length - 1;
        for (int i = 0; i < arrSize; i++) {
            if (count1 > size1 && count2 <= size2) {
                arr[i] = arr2[count2];
                count2++;
            } else if (count2 > size2 && count1 <= size1) {
                arr[i] = arr1[count1];
                count1++;
            } else if (arr1[count1].compareTo(arr2[count2]) > 0) {
                arr[i] = arr2[count2];
                count2++;
            } else if (arr2[count2].compareTo(arr1[count1]) >= 0) {
                arr[i] = arr1[count1];
                count1++;
            }
        }

        return arr;
    }

    public static <T extends Number & Comparable<T>> void insertionSort(T[] arr) {
        int arrLength = arr.length;
        for (int i = 0; i <= arrLength; i++) {
            for (int j = i - 1; j > 0; j--) {
                if (arr[j].compareTo(arr[j - 1]) < 0) {
                    swap(arr, j, j - 1);
                }
            }
         }
    }

    public static <T extends Number & Comparable<T>> void chooseSort(T[] arr) {
        int arrLength = arr.length;
        for (int i = 0; i < arrLength; i++) {
            int minIndex = searchMinElement(arr, i, arrLength);
            swap(arr, i, minIndex);
        }
    }

    private static <T extends Number & Comparable<T>> int searchMinElement(T[] arr, int start, int end) {
        int result = start;
        for (int i = start; i < end; i++ ) {
            if (arr[i].compareTo(arr[result]) < 0) {
                result = i;
            }
        }
        return result;
    }

    public static <T extends Number & Comparable<T>> void bubbleSort(T[] arr) {
        boolean moved = true;
        int arrLength = arr.length;
        while (moved) {
            moved = false;
            for (int i = 1; i < arrLength; i++) {
                if (arr[i - 1].compareTo(arr[i]) > 0) {
                    moved = true;
                    swap(arr, i - 1, i);
                }
            }
        }
    }

    public static <T extends Number & Comparable<T>> void quickSort(T[] arr) {
        qSort(arr, 0, arr.length - 1);
    }

    private static <T extends Number & Comparable<T>> void qSort(T[] arr, int firstIndex, int lastIndex) {
        if (firstIndex >= lastIndex) {
            return;
        }
        int middleIndex = firstIndex + (lastIndex - firstIndex) / 2;
        int i = firstIndex;
        int j = lastIndex;

        while (i < j) {
            while (i < middleIndex && arr[i].compareTo(arr[middleIndex]) <= 0) {
                i++;
            }
            while (j > middleIndex && arr[j].compareTo(arr[middleIndex]) > 0) {
                j--;
            }
            if (i < j) {
                swap(arr, i, j);
                if (i == middleIndex) {
                    middleIndex = j;
                } else if (j == middleIndex) {
                    middleIndex = i;
                }
            }
        }
        qSort(arr, firstIndex, middleIndex - 1);
        qSort(arr, middleIndex + 1, lastIndex);
    }

    private static <T> void swap(T[] arr, int firstElement, int lastElement) {
        T temp = arr[firstElement];
        arr[firstElement] = arr[lastElement];
        arr[lastElement] = temp;
    }
}
