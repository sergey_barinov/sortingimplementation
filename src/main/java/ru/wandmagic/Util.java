package ru.wandmagic;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Util {
    public static void fibonachi() {
        int first = 1;
        int second = 1;
        for (int i = 0; i <= 100; i++) {
            int result = first + second;
            first = second;
            second = result;
            System.out.println(result);

        }
    }

    public static void copyFile() {
        Path source = Paths.get("C:\\temp\\1\\test.txt");
        Path dest = Paths.get("C:\\temp\\2\\test.txt");
        try {
            Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
}
